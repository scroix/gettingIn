$(document).ready(function(){ 
  document.oncontextmenu = function() {return false;};

  $('#inverted_div1').mousedown(function(e) {
    $.get( "/inverted_squares", { section: "TopLeft" } );
    $(this).fadeOut("slow", function() {
      $(this).fadeIn("slow");
    });
  });   

  $('#inverted_div2').mousedown(function(e) {
    $.get( "/inverted_squares", { section: "TopRight" } );
    $(this).fadeOut("slow", function() {
      $(this).fadeIn("slow");
    });
  });  

  $('#inverted_div3').mousedown(function(e) {
    $.get( "/inverted_squares", { section: "BottomLeft" } );
    $(this).fadeOut("slow", function() {
      $(this).fadeIn("slow");
    });
  });  

  $('#inverted_div4').mousedown(function(e) {
      window.open("/video", "_self");
      return false;
  });

  $(document).keypress(function(e) {
      if(e.which == 13) {
          window.open("/", "_self");
          return false;
      }
  });  

});

