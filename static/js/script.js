$(document).ready(function(){ 
  document.oncontextmenu = function() {return false;};

  $('#div1').mousedown(function(e) {
    $.get( "/squares", { section: "TopLeft" } );
    $(this).fadeOut("slow", function() {
      $(this).fadeIn("slow");
    });
  });   

  $('#div2').mousedown(function(e) {
    $.get( "/squares", { section: "TopRight" } );
    $(this).fadeOut("slow", function() {
      $(this).fadeIn("slow");
    });
  });  

  $('#div3').mousedown(function(e) {
    $.get( "/squares", { section: "BottomLeft" } );
    $(this).fadeOut("slow", function() {
      $(this).fadeIn("slow");
    });
  });  

  $('#div4').mousedown(function(e) {
    $.get( "/squares", { section: "BottomRight" } );
    $(this).fadeOut("slow", function() {
      $(this).fadeIn("slow");
    });
  });

  $(document).keypress(function(e) {
      if(e.which == 13) {
          window.open("/inverted", "_self");
          return false;
      }
  });

});

