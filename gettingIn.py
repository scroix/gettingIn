import os
import threading
from flask import Flask,render_template, request,json
from nodel_stdio import *

# A list of Nodel events.
trigger_square01 = create_nodel_event('Square01', {'group': 'Main'})
trigger_square02 = create_nodel_event('Square02', {'group': 'Main'})
trigger_square03 = create_nodel_event('Square03', {'group': 'Main'})
trigger_square04 = create_nodel_event('Square04', {'group': 'Main'})

inverted_triggered_square01 = create_nodel_event('Inverted01', {'group': 'Inverted'})
inverted_triggered_square02 = create_nodel_event('Inverted02', {'group': 'Inverted'})
inverted_triggered_square03 = create_nodel_event('Inverted03', {'group': 'Inverted'})
inverted_triggered_square04 = create_nodel_event('Inverted04', {'group': 'Inverted'})

class Main:
    app = Flask(__name__)

    # Launch Nodel bridge on a seperate thread to Flask server.
    def thread_bridge(self):
        t = threading.Thread(target=start_nodel_channel)
        t.daemon = True
        t.start()

    @app.route('/')
    def gettingIn():
        print 'Rendering main page.'
        return render_template('gettingIn.html')

    @app.route('/inverted')
    def gettingInInverted():
        print 'Rendering inverted page.'
        return render_template('gettingIn-inverted.html')

    @app.route('/video')
    def gettingInVideo():
        print 'Rendering video page.'
        return render_template('gettingIn-video.html')

    # Identify triggered square based on data passed from jQuery function.
    @app.route('/squares')
    def identify_squares():
        square = request.args.get("section")
        if square == "TopLeft":
            trigger_square01.emit()
        if square == "TopRight":
            trigger_square02.emit()
        if square == "BottomLeft":
            trigger_square03.emit()
        if square == "BottomRight":
            trigger_square04.emit()
        return 'Noun'

    # Secondary example series of identification + events.
    @app.route('/inverted_squares')
    def identify_inverted_squares():
        square = request.args.get("section")
        if square == "TopLeft":
            inverted_triggered_square01.emit()
        if square == "TopRight":
            inverted_triggered_square02.emit()
        if square == "BottomLeft":
            inverted_triggered_square03.emit()
        if square == "BottomRight":
            inverted_triggered_square04.emit()
        return 'Noun'

    def __init__(self):

        self.thread_bridge()
        self.app.run()

main = Main()
register_instance_node(main)
